package ch.fhnw.controller;

import ch.fhnw.business.EmployeeEJB;
import ch.fhnw.domain.Employee;

import javax.ejb.EJB;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.util.ArrayList;
import java.util.List;

@ManagedBean
@SessionScoped
public class EmployeeController{

    // Beans

    @EJB
    private EmployeeEJB employeeEJB;

    /**
     * This method finds all employees
     *
     * @return list of employees
     */
    public List<Employee> findEmployees() {
        return employeeEJB.findEmployees();
    }
}
