package ch.fhnw.controller;

import ch.fhnw.business.CourseRunEJB;
import ch.fhnw.business.EmployeeEJB;
import ch.fhnw.domain.CourseRun;
import ch.fhnw.domain.Employee;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.util.ArrayList;
import java.util.List;

@ManagedBean
@SessionScoped
public class CourseRunController {

    // Beans

    @EJB
    private CourseRunEJB courseRunEJB;

    @EJB
    private EmployeeEJB employeeEJB;

    // attributes

    private CourseRun courseRun = new CourseRun();
    private int selectedEmployee = 0;

    /**
     * This method finds all course runs
     *
     * @return list of course runs
     */
    public List<CourseRun> findCourseRuns() {
        return courseRunEJB.findCourseRuns();
    }

    /**
     * This method adds a employee to a course run
     */
    public void addEmployee() {
        Employee employee = employeeEJB.findEmployee(selectedEmployee);

        if (courseRun.getEmployees() == null) {
            courseRun.setEmployees(new ArrayList<>());
        }

        courseRun.getEmployees().add(employee);
    }

    /**
     * This method creates a new course run
     *
     * @return redirect to course run
     */
    public String createCourseRun() {
        this.courseRun = new CourseRun();
        return "toCourseRun";
    }

    /**
     * This method loads a course run by a given id to edit it
     *
     * @param id of course run
     * @return redirect to course run
     */
    public String editCourseRun(int id) {
        courseRun = courseRunEJB.findCourseRun(id);
        return "toCourseRun";
    }

    /**
     * This method saves a course run
     *
     * @return redirect to course run list
     */
    public String saveCourseRun() {
        courseRunEJB.saveCourse(courseRun);
        return "toCourseRunList";
    }

    /**
     * This method deletes a course run by given id
     *
     * @param id of course run
     */
    public void deleteCourseRun(int id) {
        courseRunEJB.deleteCourse(id);
    }

    /**
     * This method deletes an employee of a course run
     *
     * @param id of employee
     */
    public void deleteEmployee(int id) {
        courseRun.getEmployees().removeIf(s -> s.getId() == id);
        saveCourseRun();
    }

    // getters/setters

    public CourseRun getCourseRun() {
        return courseRun;
    }

    public void setCourseRun(CourseRun courseRun) {
        this.courseRun = courseRun;
    }

    public int getSelectedEmployee() {
        return selectedEmployee;
    }

    public void setSelectedEmployee(int selectedEmployee) {
        this.selectedEmployee = selectedEmployee;
    }
}
