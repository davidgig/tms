package ch.fhnw.controller;

import ch.fhnw.business.CourseEJB;
import ch.fhnw.business.SkillEJB;
import ch.fhnw.domain.Course;
import ch.fhnw.domain.Skill;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.event.AjaxBehaviorEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@ManagedBean
@SessionScoped
public class CourseController {

    // Beans

    @EJB
    private CourseEJB courseEJB;

    @EJB
    private SkillEJB skillEJB;

    // attributes

    private Course course = new Course();
    private int selectedSkill = 0;

    /**
     * This method finds all courses
     *
     * @return list of courses
     */
    public List<Course> findCourses() {
        return courseEJB.findCourses();
    }

    /**
     * This method adds a skill to a course
     */
    public void addSkill() {
        Skill skill = skillEJB.findSkill(selectedSkill);

        System.out.println(selectedSkill);

        if(course.getSkills() == null) {
            course.setSkills(new ArrayList<>());
        }

        course.getSkills().add(skill);
    }

    /**
     * This method creates a new course
     *
     * @return redirect to course
     */
    public String createCourse() {
        course = new Course();
        return "toCourse";
    }

    /**
     * This method loads a course by a given id to edit it
     *
     * @param id of course
     * @return redirect to course
     */
    public String editCourse(int id) {
        course = courseEJB.findCourse(id);
        return "toCourse";
    }

    /**
     * This method saves a course
     *
     * @return redirect to course list
     */
    public String saveCourse() {
        courseEJB.saveCourse(course);
        return "toCourseList";
    }

    /**
     * This method deletes a course by a given id
     *
     * @param id of course
     */
    public void deleteCourse(int id) {
        courseEJB.deleteCourse(id);
    }

    /**
     * This method deletes a skill of a course
     *
     * @param id of skill
     */
    public void deleteSkill(int id) {
        course.getSkills().removeIf(s -> s.getId() == id);
        saveCourse();
    }

    // getters/setters

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public int getSelectedSkill() {
        return selectedSkill;
    }

    public void setSelectedSkill(int selectedSkill) {
        this.selectedSkill = selectedSkill;
    }
}
