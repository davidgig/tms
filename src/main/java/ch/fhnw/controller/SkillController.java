package ch.fhnw.controller;

import ch.fhnw.business.SkillEJB;
import ch.fhnw.domain.Skill;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.util.List;

@ManagedBean
@SessionScoped
public class SkillController {

    // Beans

    @EJB
    private SkillEJB skillEJB;

    /**
     * This method finds all skills
     *
     * @return list of skills
     */
    public List<Skill> findSkills() {
        return skillEJB.findSkills();
    }
}
