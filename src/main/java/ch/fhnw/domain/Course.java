package ch.fhnw.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@NamedQueries({
        @NamedQuery(name = "findAllCourses", query = "SELECT c FROM Course c")
})
public class Course implements Serializable {

    // attributes

    @Id
    @GeneratedValue
    private int id;
    private String name;
    private String description;

    @ManyToMany(fetch=FetchType.EAGER)
    @JoinTable(name="course_skill", joinColumns={@JoinColumn(name="course_id")}, inverseJoinColumns={@JoinColumn(name="skill_id")})
    private List<Skill> skills;

    // getters/setters

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Skill> getSkills() {
        return skills;
    }

    public void setSkills(List<Skill> skills) {
        this.skills = skills;
    }
}
