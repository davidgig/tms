package ch.fhnw.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@NamedQueries({
        @NamedQuery(name = "findAllSkills", query = "SELECT s FROM Skill s")
})
public class Skill implements Serializable {

    // attributes

    @Id
    @GeneratedValue
    private int id;
    private String name;

    @ManyToMany(mappedBy = "skills")
    private List<Course> courses;

    // getters/setters

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
