package ch.fhnw.business;

import ch.fhnw.domain.Employee;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

@Stateless
public class EmployeeEJB {

    @PersistenceContext(unitName = "tms")
    private EntityManager em;

    /**
     * This method finds all employees
     *
     * @return list of employees
     */
    public List<Employee> findEmployees() {
        TypedQuery<Employee> query = em.createNamedQuery("findAllEmployees", Employee.class);
        return query.getResultList();
    }

    /**
     * This method finds a employee by a given id
     *
     * @param id of employee
     * @return employee object
     */
    public Employee findEmployee(int id) {
        return em.find(Employee.class, id);
    }
}
