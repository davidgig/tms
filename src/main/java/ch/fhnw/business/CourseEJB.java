package ch.fhnw.business;

import ch.fhnw.domain.Course;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

@Stateless
public class CourseEJB {

    @PersistenceContext(unitName = "tms")
    private EntityManager em;

    /**
     * This method finds all courses
     *
     * @return list of courses
     */
    public List<Course> findCourses() {
        TypedQuery<Course> query = em.createNamedQuery("findAllCourses", Course.class);
        return query.getResultList();
    }

    /**
     * This method finds a course by a given id
     *
     * @param id of course
     * @return course object
     */
    public Course findCourse(int id) {
        return em.find(Course.class, id);
    }

    /**
     * This method saves a course
     *
     * @param course object
     */
    public void saveCourse(Course course) {
        em.merge(course);
    }

    /**
     * This method deletes a course by a given id
     *
     * @param id of course
     */
    public void deleteCourse(int id) {
        em.remove(em.find(Course.class, id));
    }
}
