package ch.fhnw.business;

import ch.fhnw.domain.Skill;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

@Stateless
public class SkillEJB {

    @PersistenceContext(unitName = "tms")
    private EntityManager em;

    /**
     * This method finds all skills
     *
     * @return list of skills
     */
    public List<Skill> findSkills() {
        TypedQuery<Skill> query = em.createNamedQuery("findAllSkills", Skill.class);
        return query.getResultList();
    }

    /**
     * This method finds a skill by a given id
     *
     * @param id of skill
     * @return skill object
     */
    public Skill findSkill(int id) {
        return em.find(Skill.class, id);
    }
}
