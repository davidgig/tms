package ch.fhnw.business;

import ch.fhnw.domain.CourseRun;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

@Stateless
public class CourseRunEJB {

    @PersistenceContext(unitName = "tms")
    private EntityManager em;

    /**
     * This method finds all course runs
     *
     * @return list of course runs
     */
    public List<CourseRun> findCourseRuns() {
        TypedQuery<CourseRun> query = em.createNamedQuery("findAllCourseRuns", CourseRun.class);
        return query.getResultList();
    }

    /**
     * This method finds a course run by a given id
     *
     * @param id of course run
     * @return course run object
     */
    public CourseRun findCourseRun(int id) {
        return em.find(CourseRun.class, id);
    }

    /**
     * This method saves a course run
     *
     * @param courseRun object
     */
    public void saveCourse(CourseRun courseRun) {
        em.merge(courseRun);
    }

    /**
     * This method deletes a course run by a given id
     *
     * @param id of course run
     */
    public void deleteCourse(int id) {
        em.remove(em.find(CourseRun.class, id));
    }
}
